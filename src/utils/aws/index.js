// @flow

import S3 from 'aws-sdk/clients/s3';
import { AWS_ACCESS_ID, AWS_SECRET_KEY } from '../../config';

/*
  These are wrappers around various AWS S3 bucket methods, making them async-await friendly, so 
  that they can be better used elsewhere.
*/

export const s3 = new S3({
  accessKeyId: AWS_ACCESS_ID,
  secretAccessKey: AWS_SECRET_KEY
});

// https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#deleteObject-property
export async function deleteAWSObject(params: AwsS3MethodParameters): Promise<any> {
  return new Promise((resolve, reject) => {
    s3.deleteObject(params, (error, data) => {
      if (error) {
        return reject(error);
      }
      return resolve(data);
    });
  });
}

// https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#deleteObjects-property
export async function deleteAWSObjects(params: AwsS3DeleteObjectsMethodParameters): Promise<any> {
  return new Promise((resolve, reject) => {
    s3.deleteObjects(params, (error, data) => {
      if (error) {
        return reject(error);
      }
      return resolve(data);
    });
  });
}

// https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#getObject-property
export async function getAWSObject(params: AwsS3MethodParameters): Promise<any> {
  return new Promise((resolve, reject) => {
    s3.getObject(params, (error, data) => {
      if (error) {
        return reject(error);
      }
      return resolve(data);
    });
  });
}

// https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#listObjects-property
export async function listAWSObjects(params: AwsS3MethodParameters): Promise<any> {
  return new Promise((resolve, reject) => {
    s3.listObjects(params, (error, data) => {
      if (error) {
        return reject(error);
      }
      return resolve(data);
    });
  });
}

// https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/S3.html#upload-property
export async function putAWSObject(params: AwsS3MethodParameters): Promise<any> {
  return new Promise((resolve, reject) => {
    s3.upload(params, (error, data) => {
      if (error) {
        return reject(error);
      }
      return resolve(data);
    });
  });
}

export default {
  deleteAWSObject,
  deleteAWSObjects,
  getAWSObject,
  listAWSObjects,
  putAWSObject,
  s3
};
