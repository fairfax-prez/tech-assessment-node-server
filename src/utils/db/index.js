/*
  Utils - Database

  A set of utilities to facilitate easy access to an application's postgres database.
*/

import { Pool } from 'pg';
import { DEBUG, DATABASE_URL, LOCAL_DEV_ENVIRONMENT } from '../../config';

//
// Setup pool
// -----------------------------------------------------------------------------

const pool = new Pool({
  connectionString: `${DATABASE_URL}?sslmode=require`,
  idleTimeoutMillis: 10000,
  max: 20,
  ssl: !LOCAL_DEV_ENVIRONMENT
});

process.on('exit', () => {
  pool.end();
});

//
// Query types
// -----------------------------------------------------------------------------

/*
  Select

  @param { Object } settings - Object describing the query to be executed, with the following 
  properties. All properties but table and attributes are optional.
    @prop { String } table - The name of the table to be queried
    @prop { Array } attributes - Array of strings denoting attributes to be retrieved
    @prop { String } distinct - The query's distinct (or distinct on) clause
    @prop { String } where - The query's where clause
    @prop { String } orderBy - The query's order by clause
    @prop { String } groupBy - The query's group by clause
    @prop { Number } limit - Number denoting limit to number of rows query will return
*/

export async function select(settings) {
  ensureQueryHasRequiredSettings('select', settings);

  const query = `
    SELECT 
      ${{}.hasOwnProperty.call(settings, 'distinct') ? settings.distinct : ''}
      ${settings.attributes.toString()}
      FROM 
      ${settings.table} 
      ${{}.hasOwnProperty.call(settings, 'where') ? `WHERE ${settings.where}` : ''}
      ${{}.hasOwnProperty.call(settings, 'groupBy') ? `GROUP BY ${settings.groupBy}` : ''}
      ${{}.hasOwnProperty.call(settings, 'orderBy') ? `ORDER BY ${settings.orderBy}` : ''}
      ${{}.hasOwnProperty.call(settings, 'limit') ? `LIMIT ${settings.limit}` : ''};
  `;

  try {
    return await execute(query);
  } catch (err) {
    throw err; // Error is logged in 'Execute query' function
  }
}

/*
  Update

  @param { Object } settings - Object describing the query to be executed, with the following 
  properties. All properties but table and attributesToValues are optional.
    @prop { String } table - The name of the table to be queried
    @prop { Object } attributesToValues - Object mapping attribute names to new attribute values
    @prop { String } where - The query's where clause
    @prop { Array } returning - Array of strings denoting attributes whose values are to be returned
    after the update operation is complete
*/

export async function update(settings) {
  ensureQueryHasRequiredSettings('update', settings);

  const query = `
    UPDATE 
      ${settings.table}
      ${getSetClause(settings)}
      ${{}.hasOwnProperty.call(settings, 'where') ? `WHERE ${settings.where}` : ''}
      ${
        {}.hasOwnProperty.call(settings, 'returning')
          ? `RETURNING ${settings.returning.toString()}`
          : ''
      };
  `;
  try {
    return await execute(query);
  } catch (err) {
    throw err; // Error is logged in 'Execute query' function
  }

  /*
    Get set clause
  */

  function getSetClause() {
    const attributes = Object.keys(settings.attributesToValues);
    const isAMathsExpression = / (\+|-|\/|\*) \d$/;

    let setClause = 'SET ';

    for (let i = 0; i < attributes.length; i += 1) {
      const value = settings.attributesToValues[attributes[i]];
      // If a maths expression, we need no speech marks around the variable
      if (isAMathsExpression.test(value)) {
        setClause += ` ${attributes[i]} = ${value}`;
      } else {
        setClause += ` ${attributes[i]} = '${value}'`;
      }

      // Unless last attribute, add comma to clause
      if (i < attributes.length - 1) {
        setClause += ',';
      }
    }
    return setClause;
  }
}

/*
  Insert

  @param { Object } settings - Object describing the query to be executed, with the following 
  properties. All properties but table and attributesToValues are optional. If the 'upsert' setting
  is set to true, the 'primaryKeyAttributes' also required.
    @prop { String } table - The name of the table to be queried
    @prop { Object } attributesToValues - Object mapping attribute names to new attribute values
    @prop { Array } returning - Array of strings denoting attributes whose values are to be returned
    after the update operation is complete
*/

export async function insert(settings) {
  ensureQueryHasRequiredSettings('insert', settings);

  const attributes = Object.keys(settings.attributesToValues);
  const values = [];
  for (let i = 0; i < attributes.length; i += 1) {
    // `Object.values()` exists, but can't guarantee the order of attribute values will be the same
    // as in `Object.keys()`, so we must do this
    values.push(`'${settings.attributesToValues[attributes[i]]}'`);
  }

  const query = `
    INSERT INTO
      ${settings.table}
      (${attributes.toString()})
      VALUES 
      (${values.toString()})
      ${getOnConflictClause()}
      ${
        {}.hasOwnProperty.call(settings, 'returning')
          ? `RETURNING ${settings.returning.toString()}`
          : ''
      };
  `;

  try {
    return await execute(query);
  } catch (err) {
    throw err; // Error is logged in 'Execute query' function
  }

  /*
    Get on conflict clause
  */

  function getOnConflictClause() {
    let onConflictClause = '';

    if (settings.upsert) {
      if (settings.onUpsertDoNothing) {
        onConflictClause += ` ON CONFLICT (${settings.primaryKeyAttributes.toString()}) DO NOTHING`;
      } else {
        onConflictClause += ` ON CONFLICT (${settings.primaryKeyAttributes.toString()}) DO UPDATE
          SET ${getAttributesToExcludedAttributes()}`;
      }
    }

    return onConflictClause;

    /*
      Get attributes to excluded attributes
    */

    function getAttributesToExcludedAttributes() {
      let result = '';
      for (let i = 0; i < attributes.length; i += 1) {
        result += `${attributes[i]} = excluded.${attributes[i]}`;
        // Unless last attribute, add a comma
        if (i !== attributes.length - 1) {
          result += ', ';
        }
      }
      return result;
    }
  }
}

/*
  Erase

  @param { Object } settings - Object describing the query to be executed, with the following 
  properties. All properties but table are optional.
    @prop { String } table - The name of the table to be queried
    @prop { String } where - The query's where clause
*/

export async function erase(settings) {
  ensureQueryHasRequiredSettings('delete', settings);

  const query = `
    DELETE FROM
    ${settings.table}
    ${{}.hasOwnProperty.call(settings, 'where') ? `WHERE ${settings.where}` : ''}
  `;

  try {
    return await execute(query);
  } catch (err) {
    throw err; // Error is logged in 'Execute query' function
  }
}

//
// Utilities
// -----------------------------------------------------------------------------

/*
  Execute query

  @param { String } query - SQL command to be executed
*/

export async function execute(query) {
  if (DEBUG) {
    console.log('[P] Database utils: Executing query: \n\n', query, '\n\n');
  }

  try {
    const client = await pool.connect();
    const result = await client.query(query);

    client.release();
    return result;
  } catch (err) {
    console.log('[ERR] Database utilities:', err);
    throw err;
  }
}

/*
  Ensure query has required settings

  @param { String } queryType - String 
  @param { Object } settings - Object describing the query to be executed, with the following 
  properties.
*/

function ensureQueryHasRequiredSettings(queryType, settings) {
  let requiredSettings;

  switch (queryType) {
    case 'select':
      requiredSettings = ['table', 'attributes'];
      break;
    case 'insert':
      if (settings.upsert) {
        requiredSettings = ['attributesToValues', 'table', 'primaryKeyAttributes'];
      } else {
        requiredSettings = ['attributesToValues', 'table'];
      }
      break;
    case 'delete':
      requiredSettings = ['table'];
      break;
    case 'update':
      requiredSettings = ['attributesToValues', 'table'];
      break;
    default:
      requiredSettings = ['table'];
  }

  const missingSettings = getMissingRequiredSettings(requiredSettings, settings);
  if (missingSettings.length > 0) {
    const message = `Attempted ${queryType} query with settings: 

      ${JSON.stringify(settings)} 

    Failed due to lack of settings ${missingSettings.toString}`;

    const error = {
      message,
      stack: null
    };

    throw error;
  }
}

/*
  Get missing required settings

  @param { Array } required - List of settings required by query
  @param { Object } settings - Object describing a query to be executed
*/

function getMissingRequiredSettings(required, settings) {
  const settingsKeys = Object.keys(settings);
  const missingSettings = [];

  for (let i = 0; i < settingsKeys.length; i += 1) {
    if (!{}.hasOwnProperty.call(settings, settingsKeys[i])) {
      missingSettings.push(settingsKeys[i]);
    }
  }

  return missingSettings;
}

//
// Exports
// -----------------------------------------------------------------------------

export default {
  select,
  update,
  insert,
  erase,
  execute
};
