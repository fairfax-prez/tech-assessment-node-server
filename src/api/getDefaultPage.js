// @flow

import type { Request, Response } from 'express';

const GENERIC_ERROR = 'Could not retrieve data feeds.';

function getDefaultPage() {
  return  (req: Request, res: Response) => {
   
      if (!res.headersSent) {
        res.status(200);
      }

      const msg = "Hello From Tech Assessment Node Server";
      res.write(msg);

      return res.end();
  };
}

export default getDefaultPage;
