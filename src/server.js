// Express requirements
import bodyParser from 'body-parser';
import compression from 'compression';
import cors from 'cors';
import express from 'express';
import morgan from 'morgan';
import { APP_NAME, LOCAL_DEV_ENVIRONMENT, PORT } from '../src/config';

import api from './api';


//
// CORS
// ------------------------------------------------------------------

const corsOptions = {
  origin: [
    'https://scripts.google.com',
    'https://docs.google.com'
  ]
};

if (LOCAL_DEV_ENVIRONMENT) {
  corsOptions.origin.push(/.*localhost.*/);
}

//
// Express server configuration
// ------------------------------------------------------------------

const app = express();

// Compress, parse, log, and raid the cookie jar
app.use(compression());
app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(morgan('dev'));


//
// API routes goes here
// ------------------------------------------------------------------

app.get('/', api.getDefaultPage());





//
// Errors
// ------------------------------------------------------------------

app.listen(PORT, console.log(`[S] Tech Assessment Server: Listening on port ${PORT}!`));

// Handle the bugs somehow
app.on('error', error => {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof PORT === 'string' ? 'Pipe ' + PORT : 'Port ' + PORT;

  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
});
