import dotenv from 'dotenv';

dotenv.config();

//
// General config
// ------------------------------------------------------------------

export const APP_NAME = process.env.APP_NAME;

//
// Development environment config
// ------------------------------------------------------------------

export const DEBUG = process.env.DEBUG === 'true';

//
// Server config
// ------------------------------------------------------------------

export const PORT = process.env.PORT;

//
// Datanase config
// ------------------------------------------------------------------

export const DATABASE_URL = process.env.DATABASE_URL;

//
// Local dummy auth config
// ------------------------------------------------------------------

export const LOCAL_DEV_ENVIRONMENT = process.env.LOCAL_DEV_ENVIRONMENT === 'true';

//
// AWS config
// ------------------------------------------------------------------

export const AWS_BUCKET_URL_ROOT = process.env.AWS_BUCKET_URL_ROOT;
export const AWS_BUCKET_KEY_ROOT = process.env.AWS_BUCKET_KEY_ROOT;
export const AWS_ACCESS_ID = process.env.AWS_ACCESS_ID;
export const AWS_BUCKET_NAME = process.env.AWS_BUCKET_NAME;
export const AWS_SECRET_KEY = process.env.AWS_SECRET_KEY;

export default {
  APP_NAME,
  AWS_ACCESS_ID,
  AWS_BUCKET_NAME,
  AWS_BUCKET_KEY_ROOT,
  AWS_BUCKET_URL_ROOT,
  AWS_SECRET_KEY,
  DATABASE_URL,
  DEBUG,
  LOCAL_DEV_ENVIRONMENT,
  PORT
};
