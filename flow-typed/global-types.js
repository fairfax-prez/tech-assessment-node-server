export type DataFeed = {
  data_feed_id: string,
  data_feed_name: string,
  data_feed_url: string,
  last_modified_data_in_s: number,
  user_id: string
};

type AwsS3MethodParameters = {
  ACL?: string,
  Body?: string | Buffer | Blob | $TypedArray | ReadableStream,
  Bucket: string,
  ContentType?: string,
  Key?: string,
  Prefix?: string
};

type AwsDeleteObjectsObjectSpecifiction = {
  Key: string
};

type AwsS3DeleteObjectsMethodParameters = {
  Bucket: string,
  Delete: {
    Objects: Array<AwsDeleteObjectsObjectSpecifiction>
  }
};
